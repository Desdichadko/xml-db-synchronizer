package ru.loginov.xmldbconverter.repositories;

import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

public class ConnectionTest {

    @Test
    public void shouldGetJdbcConnection() throws SQLException {
        try (Connection connection = DepartmentRepository.getConnection()) {
            assertTrue(connection.isValid(1));
            assertFalse(connection.isClosed());
        }
    }
}
