package ru.loginov.xmldbconverter.repositories;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.loginov.xmldbconverter.models.DepartmentJob;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

public class DepartmentRepository {

    private static final Logger logger = LogManager.getLogger(DepartmentRepository.class);

    private static Connection connection;

    //language=SQL
    private static final String SQL_INSERT_NEW_ROW = "INSERT INTO departments " +
            "(dep_code, dep_job, description) VALUES (?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_ALL = "SELECT dep_code, dep_job, description FROM departments";

    //language=SQL
    private static final String SQL_SELECT_ALL_AND_ORDER_BY_DEPARTMENT = "SELECT dep_code, dep_job, description " +
            "FROM departments " +
            "ORDER BY dep_code";

    //language=SQL
    private static final String SQL_UPDATE_DESCRIPTION = "UPDATE departments " +
            "SET description = ? " +
            "WHERE dep_code = ? AND dep_job = ?";

    //language=SQL
    private static final String SQL_DELETE_JOB = "DELETE FROM departments " +
            "WHERE dep_code = ? AND dep_job = ?";

    /**
     * Get connection if exist or establishing if absent
     *
     * @return connection
     */
    public static Connection getConnection() {
        if (connection == null) {
            Properties properties = new Properties();
            String url;
            String username;
            String password;
            logger.debug("Trying to establish a connection with database...");
            try {
                logger.debug("Getting properties for connecting to a database...");
                properties.load(new FileInputStream("application.properties"));
                url = properties.getProperty("application.datasource.url");
                username = properties.getProperty("application.datasource.username");
                password = properties.getProperty("application.datasource.password");
            } catch (IOException e) {
                logger.fatal("Application properties not found!", e);
                throw new IllegalArgumentException("Fatal error: application properties not found!");
            }
            try {
                connection = DriverManager.getConnection(url, username, password);
                connection.setAutoCommit(false);
            } catch (SQLException e) {
                logger.fatal("Unable to connect database!");
                throw new IllegalArgumentException("Unable to connect database!");
            }
        }
        logger.debug("Connection successfully established.");
        return connection;
    }

    /**
     * Synchronizes database with XML file
     *
     * @param depsJobsFromXml Map of all departments jobs parsed from XML file
     */
    public static void syncDatabaseWithXml(Map<DepartmentJob, String> depsJobsFromXml) {
        logger.info("Starting synchronization with database...");
        Map<DepartmentJob, String> depsJobsFromDb = getDepsJobsFromDb();
        logger.debug("Applying changes to database...");
        for (Map.Entry<DepartmentJob, String> job : depsJobsFromXml.entrySet()) {
            DepartmentJob currentJob = job.getKey();
            if (depsJobsFromDb.containsKey(currentJob)) {
                if (!depsJobsFromDb.get(currentJob).equals(job.getValue())) {
                    updateJobInDb(job.getValue(), currentJob.getDepCode(), currentJob.getDepJob());
                }
            } else {
                putNewJobToDb(currentJob.getDepCode(), currentJob.getDepJob(), job.getValue());
            }
            depsJobsFromDb.remove(currentJob);
        }
        removeJobsFromDb(depsJobsFromDb);
        try {
            logger.debug("Trying to commit changes to database.");
            connection.commit();
        } catch (SQLException e) {
            throw new IllegalArgumentException("Error: Transaction denied! Something went wrong!");
        }
        logger.info("FINISHED! XML file has been successfully synchronized with the database!");
    }

    /**
     * Gets all departments jobs from database
     *
     * @return All departments jobs as HashMap
     */
    public static Map<DepartmentJob, String> getDepsJobsFromDb() {
        logger.debug("Getting all jobs from a database...");
        Map<DepartmentJob, String> depsJobsFromDb = new HashMap<>();
        return getDepartmentsJobsMap(depsJobsFromDb, SQL_SELECT_ALL);
    }

    /**
     * Gets all departments jobs from database as ordered map
     *
     * @return All departments jobs as LinkedHashMap
     */
    public static Map<DepartmentJob, String> getOrderedDepsJobsFromDb() {
        logger.info("Getting all jobs as LinkedHashMap from a database...");
        Map<DepartmentJob, String> depsJobsFromDb = new LinkedHashMap<>();
        return getDepartmentsJobsMap(depsJobsFromDb, SQL_SELECT_ALL_AND_ORDER_BY_DEPARTMENT);
    }

    /**
     * Performs an SQL query and collects result as Map of all departments jobs
     *
     * @param depsJobsFromDb    Realisation of a map
     * @param sqlSelectAllQuery SQL query
     * @return Map of selected departments jobs
     */
    private static Map<DepartmentJob, String> getDepartmentsJobsMap(
            Map<DepartmentJob, String> depsJobsFromDb, String sqlSelectAllQuery) {
        try {
            ResultSet resultSet = connection.createStatement().executeQuery(sqlSelectAllQuery);
            logger.debug("ResultSet from database has been receiver. Processing started...");
            while (resultSet.next()) {
                String depCode = resultSet.getString("dep_code");
                String depJob = resultSet.getString("dep_job");
                String description = resultSet.getString("description");
                depsJobsFromDb.put(new DepartmentJob(depCode, depJob), description);
            }
        } catch (SQLException e) {
            logger.fatal("Unable to get data from database!", e);
            throw new IllegalArgumentException("Error: Unable to get data from database!");
        }
        logger.debug("Map of all department jobs has been created.");
        return depsJobsFromDb;
    }


    /**
     * Deletes specified jobs
     *
     * @param jobsForDeleting Jobs which has to be deleted
     */
    private static void removeJobsFromDb(Map<DepartmentJob, String> jobsForDeleting) {
        logger.debug("Deleting redundant jobs from database...");
        try {
            for (Map.Entry<DepartmentJob, String> job : jobsForDeleting.entrySet()) {
                PreparedStatement removeJobsFromDbStmt = connection.prepareStatement(SQL_DELETE_JOB);
                removeJobsFromDbStmt.setString(1, job.getKey().getDepCode());
                removeJobsFromDbStmt.setString(2, job.getKey().getDepJob());
                removeJobsFromDbStmt.execute();
            }
        } catch (SQLException e) {
            logger.error("Unable to delete redundant jobs in Database! Changes will not be applied!");
            throw new IllegalArgumentException("Error: Unable to delete redundant jobs in Database!");
        }
        logger.debug("Redundant jobs have been successfully deleted!");
    }

    /**
     * Puts a new job row in a database
     *
     * @param depCode     Code of a department
     * @param depJob      Name of department job
     * @param description Description
     */
    private static void putNewJobToDb(String depCode, String depJob, String description) {
        logger.debug("Creating a new job...");
        try {
            PreparedStatement updateDescriptionStmt = connection.prepareStatement(SQL_INSERT_NEW_ROW);
            updateDescriptionStmt.setString(1, depCode);
            updateDescriptionStmt.setString(2, depJob);
            updateDescriptionStmt.setString(3, description);
            updateDescriptionStmt.execute();
        } catch (SQLException e) {
            logger.error("Unable to put a new job to Database! Changes will not be applied!");
            throw new IllegalArgumentException("Error: Unable to put a new job to Database!");
        }
        logger.debug("New jobs have been successfully added to database.");
    }

    /**
     * Updates job description in database accordingly with XML file
     *
     * @param description Description
     * @param depCode     Code of a department
     * @param depJob      Name of department job
     */
    private static void updateJobInDb(String description, String depCode, String depJob) {
        logger.debug("Updating jobs descriptions...");
        try {
            PreparedStatement updateDescriptionStmt = connection.prepareStatement(SQL_UPDATE_DESCRIPTION);
            updateDescriptionStmt.setString(1, description);
            updateDescriptionStmt.setString(2, depCode);
            updateDescriptionStmt.setString(3, depJob);
            updateDescriptionStmt.execute();
        } catch (SQLException e) {
            logger.error("Unable to update a job description in Database! Changes will not be applied!", e);
            throw new IllegalArgumentException("Error: Unable to update a job description in Database!");
        }
        logger.debug("Job descriptions have been successfully updated.");
    }
}
