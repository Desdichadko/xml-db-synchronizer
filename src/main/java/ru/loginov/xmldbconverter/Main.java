package ru.loginov.xmldbconverter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.loginov.xmldbconverter.models.DepartmentJob;
import ru.loginov.xmldbconverter.processors.XmlBuilder;
import ru.loginov.xmldbconverter.processors.XmlParser;
import ru.loginov.xmldbconverter.repositories.DepartmentRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Map;


public class Main {

    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {

        Connection connection = DepartmentRepository.getConnection();
        switch (args[0].toLowerCase(Locale.ROOT)) {
            case "-syncff":
                Map<DepartmentJob, String> allDepsJobs = XmlParser.parse(args[1]);
                DepartmentRepository.syncDatabaseWithXml(allDepsJobs);
                break;
            case "-syncfdb":
                Map<DepartmentJob, String> depsJobsFromDb =
                        DepartmentRepository.getOrderedDepsJobsFromDb();
                XmlBuilder.getXmlFromDatabase(args[1], depsJobsFromDb);
                break;
        }
        try {
            connection.close();
        } catch (SQLException e) {
            logger.warn(e);
            e.printStackTrace();
        }
    }

}
