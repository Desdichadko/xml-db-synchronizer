package ru.loginov.xmldbconverter.processors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import ru.loginov.xmldbconverter.models.DepartmentJob;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.Map;
import java.util.Objects;

public class XmlBuilder {

    private static final Logger logger = LogManager.getLogger(XmlBuilder.class);

    /**
     * Creates XML file based on database data
     *
     * @param filePath                    Path to output file
     * @param depsJobsFromDb Map of all departments jobs received from database
     */
    public static void getXmlFromDatabase(String filePath, Map<DepartmentJob, String> depsJobsFromDb) {
        logger.info("Starting creating XML file from database data...");
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            Document document = dbf.newDocumentBuilder().newDocument();
            Element root = document.createElement("root");
            document.appendChild(root);
            String depCode = "";
            Element department = null;
            logger.debug("Creating jobs nodes...");
            for (Map.Entry<DepartmentJob, String> job : depsJobsFromDb.entrySet()) {
                if (!Objects.equals(depCode, job.getKey().getDepCode())) {
                    depCode = job.getKey().getDepCode();
                    department = document.createElement("department");
                    department.setAttribute("depcode", depCode);
                    root.appendChild(department);
                }
                assert department != null;
                department.appendChild(createJob(document, job.getKey().getDepJob(), job.getValue()));
            }
            logger.debug("Structure has been created.");

            logger.debug("Creating and formatting a file...");
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transf = transformerFactory.newTransformer();
            transf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transf.setOutputProperty(OutputKeys.INDENT, "yes");
            transf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            DOMSource source = new DOMSource(document);
            File myFile = new File(filePath);
            StreamResult file = new StreamResult(myFile);
            transf.transform(source, file);
        } catch (ParserConfigurationException | TransformerException e) {
            logger.fatal("Something went wrong while creating a file!", e);
            throw new IllegalArgumentException("Something went wrong...");
        }
        logger.info("SUCCESS: XML file has been downloaded successfully!");
    }

    /**
     * Creates a job node
     *
     * @param document    XML document
     * @param depJob      Department job name
     * @param description Description
     * @return Job node
     */
    private static Node createJob(Document document, String depJob, String description) {
        Element node = document.createElement("depjob");
        Node depJobNode = node.appendChild(document.createElement("name"));
        depJobNode.appendChild(document.createTextNode(depJob));
        Node depCodeNode = node.appendChild(document.createElement("description"));
        depCodeNode.appendChild(document.createTextNode(description));
        return node;
    }
}
