package ru.loginov.xmldbconverter.processors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import ru.loginov.xmldbconverter.models.Department;
import ru.loginov.xmldbconverter.models.DepartmentJob;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class XmlParser {

    private static final Logger logger = LogManager.getLogger(XmlParser.class);

    private static String filePath;

    private static final String TAG_DEPARTMENT = "department";
    private static final String TAG_DEPARTMENT_JOB = "depjob";
    private static final String TAG_DEPARTMENT_JOB_NAME = "name";
    private static final String TAG_DEPARTMENT_JOB_DESCRIPTION = "description";

    public static Map<DepartmentJob, String> parse(String filePath) {
        logger.info("Starting parsing XML file...");
        XmlParser.filePath = filePath;
        Document document = getDocument();
        Node rootNode = document.getFirstChild();
        NodeList departmentsNodeList = rootNode.getChildNodes();
        Set<Department> departmentsSet = parseDepartmentsSet(departmentsNodeList);
        Map<DepartmentJob, String> allDepartmentsJobs = getAllDepartmentsJobs(departmentsSet);
        logger.info("XML file has be parsed successfully.");
        return allDepartmentsJobs;
    }

    private static Map<DepartmentJob, String> getAllDepartmentsJobs(Set<Department> departmentsSet) {
        Map<DepartmentJob, String> allDepartmentsJobs = new HashMap<>();
        for (Department department : departmentsSet) {
            allDepartmentsJobs.putAll(department.getDepJobs());
        }
        return allDepartmentsJobs;
    }

    /**
     * Gets Document from XML file
     *
     * @return Document
     */
    private static Document getDocument() {
        logger.debug("Getting XML file...");
        File file;
        try {
            file = new File(filePath);
        } catch (NullPointerException e) {
            logger.fatal("File not found!");
            throw new IllegalArgumentException("File not found!");
        }
        Document document;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            logger.debug("Trying to parse data from the file...");
            document = dbf.newDocumentBuilder().parse(file);
            document.getDocumentElement().normalize();
        } catch (SAXException | IOException | ParserConfigurationException e) {
            logger.fatal("Unable to parse the file!", e);
            throw new IllegalArgumentException("Something went wrong! Unable to parse the file!");
        }
        logger.debug("Document received.");
        return document;
    }

    /**
     * Parses Set of all departments
     *
     * @param departmentsNodeList NodeList with all departments
     * @return Set of all departments
     */
    private static Set<Department> parseDepartmentsSet(NodeList departmentsNodeList) {
        logger.debug("Trying to parse departments...");
        Set<Department> departmentSet = new HashSet<>();
        for (int i = 0; i < departmentsNodeList.getLength(); i++) {
            if (departmentsNodeList.item(i).getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            if (TAG_DEPARTMENT.equals(departmentsNodeList.item(i).getNodeName())) {
                Node departmentNode = departmentsNodeList.item(i);
                if (departmentNode == null) {
                    continue;
                }
                String depCode = departmentNode.getAttributes().item(0).getNodeValue();
                NodeList jobsNodeList = departmentNode.getChildNodes();
                Department department = new Department(depCode);
                addAllDepartmentJobs(department, jobsNodeList);
                departmentSet.add(department);
            }
        }
        return departmentSet;
    }

    /**
     * Adding all department jobs
     *
     * @param department             The department
     * @param departmentJobsNodeList Node list with department jobs
     */
    private static void addAllDepartmentJobs(Department department, NodeList departmentJobsNodeList) {
        logger.debug("Trying to parse all department jobs...");
        for (int j = 0; j < departmentJobsNodeList.getLength(); j++) {
            if (TAG_DEPARTMENT_JOB.equals(departmentJobsNodeList.item(j).getNodeName())) {
                NodeList jobInfo = departmentJobsNodeList.item(j).getChildNodes();
                putJob(department, jobInfo);
            }
        }
        logger.debug("All jobs from department {} successfully received", department.getDepCode());
    }

    /**
     * Puts a job to depJob Map
     *
     * @param department Department
     * @param jobInfo    Department job's name and description
     */
    private static void putJob(Department department, NodeList jobInfo) {
        String depJob = "";
        String jobDescription = "";
        for (int k = 0; k < jobInfo.getLength(); k++) {
            switch (jobInfo.item(k).getNodeName()) {
                case TAG_DEPARTMENT_JOB_NAME -> depJob = jobInfo.item(k).getTextContent();
                case TAG_DEPARTMENT_JOB_DESCRIPTION -> jobDescription = jobInfo.item(k).getTextContent();
            }
        }
        if (!depJob.isBlank()) {
            if (department.putDepJob(depJob, jobDescription)) {
                logger.fatal("There are duplicates in XML file!");
                throw new IllegalArgumentException("There are duplicates in XML file!");
            }
        }
    }
}
