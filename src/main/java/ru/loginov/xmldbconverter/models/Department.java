package ru.loginov.xmldbconverter.models;

import java.util.HashMap;
import java.util.Map;

public class Department {

    private final String depCode;

    private final Map<DepartmentJob, String> depJobs = new HashMap<>();

    public Department(String depCode) {
        this.depCode = depCode;
    }

    public String getDepCode() {
        return depCode;
    }

    public Map<DepartmentJob, String> getDepJobs() {
        return depJobs;
    }

    /**
     * Put the job in department jobs Map
     *
     * @param name        Name of the department job
     * @param description Description
     * @return True if there are duplicate
     */
    public boolean putDepJob(String name, String description) {
        return depJobs.put(new DepartmentJob(this.depCode, name), description) != null;
    }
}
