package ru.loginov.xmldbconverter.models;

import java.util.Objects;

public class DepartmentJob {

    private String depCode;

    private String depJob;


    public DepartmentJob(String depCode, String depJob) {
        this.depCode = depCode;
        this.depJob = depJob;
    }

    public String getDepCode() {
        return depCode;
    }

    public String getDepJob() {
        return depJob;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DepartmentJob that = (DepartmentJob) o;
        return depCode.equals(that.depCode) && depJob.equals(that.depJob);
    }

    @Override
    public int hashCode() {
        return Objects.hash(depCode, depJob);
    }

}
