DROP TABLE IF EXISTS Departments;

CREATE TABLE departments (
    id serial primary key,
    dep_code varchar(20),
    dep_job varchar(100),
    description varchar(255),
    UNIQUE (dep_code, dep_job)
);

INSERT INTO departments (dep_code, dep_job, description) VALUES ('DEV2823', 'Senior Developer', 'Builds architecture of an application, solves complex technical problems');
INSERT INTO departments (dep_code, dep_job, description) VALUES ('ADM9214', 'Accountant', 'Accounts money, pays taxes');
INSERT INTO departments (dep_code, dep_job, description) VALUES ('DEV2823', 'Middle Developer', 'Independently solves problems of an average level');
INSERT INTO departments (dep_code, dep_job, description) VALUES ('ADM9214', 'HR manager', 'Looks for new employees');
INSERT INTO departments (dep_code, dep_job, description) VALUES ('MRK4691', 'Marketer', 'Advertises services');
INSERT INTO departments (dep_code, dep_job, description) VALUES ('ADM9214', 'Lawyer', 'Represents the law, drafts contracts');
INSERT INTO departments (dep_code, dep_job, description) VALUES ('DEV2823', 'Junior Developer', 'Dusturb middle and senior developers)');
INSERT INTO departments (dep_code, dep_job, description) VALUES ('MRK4691', 'Analyst', 'Collects data and analyze it');