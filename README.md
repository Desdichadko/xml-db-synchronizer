# XML-DB synchronizer

## Settings:

* [application.properties](src/main/resources/application.properties) - database settings file

* [log4j2.xml](src/main/resources/log4j2.xml) - log4j settings file where *logger.log* file path to logs file
    ```
    <Property name="filename">logger.log</Property>
    ```


## Input arguments:
1) type of function:

    *-syncff* - synchronize database from XML file

    *-syncfdb* - create XML file based on database data

2) XML file from which/to which synchronization will be done, for example: *test.xml*


## How to launch:
1) Download assembled project from: https://disk.yandex.ru/d/6Z_1_l8uU_qxNQ
2) It is **necessary** to configure the database in ***application.properties*** located near .jar file
3) Launch *xml-db-synchronizer.bat*

**Optional**: you can specify output path for .log file. For this open .jar file with Winrar and configure file log4j2.xml located in the root (*logger.log* by default).

